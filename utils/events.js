import axios from 'axios'
import { getToken, isLogged } from './auth'

const BASE_URL = 'http://localhost:3000'

export { getEventsForUser, createEvent }

function getEventsForUser() {
  return new Promise((resolve, reject) => {
    if (!isLogged()) {
      console.error("Necesita estar logado para llevar a cabo esta operación")
      reject("You need to be logged")
    }

    const eventsURL = `${BASE_URL}/users/getEvents`

    axios.get(eventsURL, { headers: { 'token': getToken() } })
      .then(response => {
        resolve(response.data.item)
      })
      .catch(error => {
        const errorResponse = error.response
        if (errorResponse) {
          // The request was made and the server responded with a status code
          console.error('Código de error', errorResponse.status, 'Error:', errorResponse.data.reason);
          reject(errorResponse.data.reason);
        } else if (error.request) {
          reject(error.request)
          // The request was made but no response was received
        } else {
          // Something happened in setting up the request that triggered an Error
          reject('Error', error.message)
        }
      })
  })
}

function createEvent(params) {
  return new Promise((resolve, reject) => {
    if (!isLogged()) {
      console.error("Necesita estar logado para llevar a cabo esta operación")
      reject("You need to be logged")
    }

    const createEventURL = `${BASE_URL}/events/create`

    axios.post(createEventURL, params, { headers: { 'token': getToken() } })
      .then(response => {
        resolve(response.data.item)
      })
      .catch(error => {
        const errorResponse = error.response
        if (errorResponse) {
          // The request was made and the server responded with a status code
          console.error('Código de error', errorResponse.status, 'Error:', errorResponse.data.reason);
          reject(errorResponse.data.reason);
        } else if (error.request) {
          reject(error.request)
          // The request was made but no response was received
        } else {
          // Something happened in setting up the request that triggered an Error
          reject('Error', error.message)
        }
      })
  })
}
