import axios from 'axios';

const BASE_URL = 'http://localhost:3000/users'

export { login, logout, getToken, isLogged }

function login(email, password) {
  const loginURL = `${BASE_URL}/login`;
  return new Promise((resolve, reject) => {
    return axios.post(loginURL, { email, password })
    .then(response => {
      console.log(JSON.stringify(response))
      const userData = response.data.item
      //localStorage.setItem('userToken', userData.token)
      localStorage.setItem('userLogged', JSON.stringify(userData))
      resolve(userData)
    })
    .catch(error => {
      const errorResponse = error.response
      reject(error);
      if (errorResponse) {
        // The request was made and the server responded with a status code
        console.error('Código de error', errorResponse.status, 'Error:', errorResponse.data.reason);
      } else if (error.request) {
        console.error(error.request)
        // The request was made but no response was received
      } else {
        // Something happened in setting up the request that triggered an Error
        console.error('Error', error.message)
      }
    });
  });
}

function logout() {
  const logoutURL = `${BASE_URL}/logout`
  return axios.get(logoutURL).then(response => response.item)
}

function getToken() {
  const userLogged = getUserLoged()
  return userLogged.token;
}

function getUserLoged() {
  return JSON.parse(localStorage.getItem('userLogged'));
}

function isLogged() {
  return getUserLoged() != null;
}
