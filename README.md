# testing-vue

> A vue example proyect

En las próximas versiones hay que trabajar en la validación de formularios
y en la creación de componentes comunes como listas de eventos o alert,
para reusar el código y no repetirlo.

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
