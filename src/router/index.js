import Vue from 'vue'
import Router from 'vue-router'
import AppNav from '@/components/AppNav'
import MeetUpList from '@/components/MeetUpList'
import LoginPage from '@/components/LoginPage'
import PrivateOptions from '@/components/UserPrivateMenu'
import MyEvents from '@/components/MyEvents'
import CreateEvent from '@/components/addEvent'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'AppNav',
      component: AppNav
    },
    {
      path: '/events',
      name: 'EventsList',
      component: MeetUpList
    },
    {
      path: '/login',
      name: 'Login',
      component: LoginPage
    },
    {
      path: '/privateOptions',
      name: 'Private Options',
      component: PrivateOptions
    },
    {
      path: '/myEvents',
      name: 'My Events',
      component: MyEvents
    },
    {
      path: '/createEvent',
      name: 'Create Event',
      component: CreateEvent
    }
  ]
})
